<?php

namespace App\Http\Controllers;

use App\Models\Kajianstudy;
use App\Http\Requests\StoreKajianstudyRequest;
use App\Http\Requests\UpdateKajianstudyRequest;

class KajianstudyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreKajianstudyRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreKajianstudyRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Kajianstudy  $kajianstudy
     * @return \Illuminate\Http\Response
     */
    public function show(Kajianstudy $kajianstudy)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Kajianstudy  $kajianstudy
     * @return \Illuminate\Http\Response
     */
    public function edit(Kajianstudy $kajianstudy)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateKajianstudyRequest  $request
     * @param  \App\Models\Kajianstudy  $kajianstudy
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateKajianstudyRequest $request, Kajianstudy $kajianstudy)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Kajianstudy  $kajianstudy
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kajianstudy $kajianstudy)
    {
        //
    }
}
